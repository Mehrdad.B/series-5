package sbu.cs;

public class BlackFunctions {
    public static String Function1(String txt1)
    {
        StringBuilder rev = new StringBuilder(txt1);
        return rev.reverse().toString();
    }
    public static String Function2(String txt1)
    {
        StringBuilder repeate = new StringBuilder("");
        for(int i=0 ; i<txt1.length() ; i++)
        {
            repeate.append(String.valueOf(txt1.charAt(i)) + String.valueOf(txt1.charAt(i))) ;
        }
        return repeate.toString();
    }
    public static String Function3(String txt1)
    {
        return txt1+txt1;
    }
    public static String Function4(String txt1)
    {
        String shift = txt1.substring(0 , txt1.length()-1);
        String shifted = String.valueOf(txt1.charAt(txt1.length()-1)) + shift;
        return shifted;
    }
    public static String Function5(String txt1)
    {
        StringBuilder replacement = new StringBuilder(txt1);
        for(int i=0 ; i<txt1.length() ; i++)
        {
            replacement.replace(i , i+1 , String.valueOf((char) (219 - txt1.charAt(i))));
        }
        return replacement.toString();
    }
}
