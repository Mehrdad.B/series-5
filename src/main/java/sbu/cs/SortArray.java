package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0 ;i< size-1; i++){
            int min = i;

            for (int j = i+1; j<size; j++){
                if (arr[j] < arr[min]){
                    min = j;
                }
            }
            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
            int tmp = arr[i];
            int index = i-1;
            while ( (index > -1) && ( arr [index] > tmp ) ) {
                arr [index+1] = arr [index];
                index--;
            }
            arr[index+1] = tmp;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        if (size==1)
        {
            return arr;
        }
        int leftside = size/2 , rightside = (size+1)/2;
        int[] leftarr = new int[leftside] ;
        int[] rightarr = new  int[rightside];
//        int index2 = 0;
//        int index = 0;
        for (int i=0 ; i<leftside ; i++)
        {
            leftarr[i] = arr[i];
        }
        for (int i=0 ; i<rightside ; i++)
        {
            rightarr[i] = arr[leftside + i];
        }
        leftarr = mergeSort(leftarr , leftside);
        rightarr = mergeSort(rightarr , rightside);
        int[] finalanswer = new int[size];
        int leftindex = 0 , rightindex = 0 , mainindex = 0 ;
        while ( leftindex < leftside && rightindex < rightside)
        {
            if ( leftarr[leftindex] <= rightarr[rightindex])
            {
                finalanswer[mainindex] = leftarr[leftindex];
                mainindex++;
                leftindex++;
            }
            else
            {
                finalanswer[mainindex] = rightarr[rightindex];
                mainindex++;
                rightindex++;
            }
        }
        if ( leftindex < leftside)
        {
            for (int i=leftindex ; i<leftside ; i++)
            {
                finalanswer[mainindex] = leftarr[i];
                mainindex++;
            }
        }
        else
        {
            for (int i=rightindex ; i<rightside ; i++)
            {
                finalanswer[mainindex] = rightarr[i];
                mainindex++;
            }
        }
        return finalanswer;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int startindex = 0;
        int endingindex = arr.length;
        while (endingindex - startindex > 1)
        {
            if (value < arr[(startindex + endingindex)/2])
            {
                endingindex = (startindex + endingindex)/2;
            }
            else
            {
                startindex = (startindex + endingindex)/2;
            }
        }
        if (arr[startindex] == value)
            return startindex;

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return binarySearch(arr , 0 , arr.length , value);
    }
    int binarySearch(int arr[], int left , int right , int key) {
        if (right >= left) {
            int mid = left + (right - left) / 2;
            if (arr[mid] == key)
                return mid;

            if (arr[mid] > key)
                return binarySearch(arr, left, mid - 1, key);

            return binarySearch(arr, mid + 1, right, key);
        }

        return -1;
    }
}
