package sbu.cs;

public class BlueSquare extends Square{
    public BlueSquare(int functionNumber) {
        super(functionNumber);
    }
    private String upinput;
    private String leftinput;
    private String rightoutput;
    private String downoutput;
    public void setupinput(String input)
    {
        upinput=input;
    }
    public void setleftinput(String input)
    {
        leftinput=input;
    }
    public String getUpinput()
    {
        return upinput;
    }
    public String getleftinput()
    {
        return leftinput;
    }
    public String stractionup()
    {
        switch (super.getNumber())
        {
            case 1 : downoutput = BlackFunctions.Function1(getUpinput());
                break;
            case 2 : downoutput = BlackFunctions.Function2(getUpinput());
                break;
            case 3 : downoutput = BlackFunctions.Function3(getUpinput());
                break;
            case 4 : downoutput = BlackFunctions.Function4(getUpinput());
                break;
            case 5 : downoutput = BlackFunctions.Function5(getUpinput());
                break;
        }
        return downoutput;
    }
    public String stractionleft()
    {
        switch (super.getNumber())
        {
            case 1 : rightoutput = BlackFunctions.Function1(getleftinput());
                break;
            case 2 : rightoutput = BlackFunctions.Function2(getleftinput());
                break;
            case 3 : rightoutput = BlackFunctions.Function3(getleftinput());
                break;
            case 4 : rightoutput = BlackFunctions.Function4(getleftinput());
                break;
            case 5 : rightoutput = BlackFunctions.Function5(getleftinput());
                break;
        }
        return rightoutput;
    }
}
