package sbu.cs;

public class WhiteFunctions {
    public static String Function1(String txt1 , String txt2)
    {
        StringBuilder mixed = new StringBuilder("");
        int size;
        String biggertxt;
        String smalertxt;
        if(txt1.length()>txt2.length())
        {
            size = txt2.length();
            biggertxt = txt1;
            smalertxt = txt2;
        }
        else
        {
            size = txt1.length();
            biggertxt = txt2;
            smalertxt = txt1;
        }
        for(int i=0 ; i<size ; i++)
        {
            mixed.append(String.valueOf(txt1.charAt(i)) + String.valueOf(txt2.charAt(i)));
        }
        return mixed.toString() + biggertxt.substring(smalertxt.length() , biggertxt.length());
    }
    public static String Function2(String txt1 , String txt2)
    {
        StringBuilder reversetxt2 = new StringBuilder(txt2);
        String ANS = txt1 + reversetxt2.reverse();
        return ANS;
    }
    public static String Function3(String txt1 , String txt2)
    {
        StringBuilder reversemixed = new StringBuilder("");
        StringBuilder reversetxt2 = new StringBuilder(txt2);
        int size;
        String biggertxt;
        String smalertxt;
        if(txt1.length()>txt2.length())
        {
            size = txt2.length();
            biggertxt = txt1;
            smalertxt = txt2;
        }
        else
        {
            size = txt1.length();
            biggertxt = txt2;
            smalertxt = txt1;
        }
        for(int i=0 ; i<size ; i++)
        {
            reversemixed.append(String.valueOf(txt1.charAt(i)) + String.valueOf(reversetxt2.charAt(txt2.length()-1-i)));
        }
        StringBuilder finalans = new StringBuilder(biggertxt);
        return  reversemixed.toString() + finalans.reverse().substring(smalertxt.length()  , biggertxt.length());
    }
    public static String Function4(String txt1 , String txt2)
    {
        if(txt1.length()%2==0)
            return txt1;
        else
            return txt2;

    }
    public static String Function5(String txt1 , String txt2)
    {
        StringBuilder mixed = new StringBuilder("");
        int size;
        String biggertxt;
        String smalertxt;
        if(txt1.length()>txt2.length())
        {
            size = txt2.length();
            biggertxt = txt1;
            smalertxt = txt2;
        }
        else
        {
            size = txt1.length();
            biggertxt = txt2;
            smalertxt = txt1;
        }
        for(int i=0 ; i<size ; i++)
        {
            char tmp = (char) ((char) (txt1.charAt(i) + txt2.charAt(i) - 2*'a')%26 + 'a');
            mixed.append(String.valueOf(tmp));
        }
        String ans;
        return mixed + biggertxt.substring(smalertxt.length()  , biggertxt.length());
    }
}
