package sbu.cs;

public class GreenSquare extends Square{

    public GreenSquare(int functionNumber) {
        super(functionNumber);
    }
    private String input;
    private String output;
    public void setinput(String input)
    {
        this.input=input;
    }
    public String getinput()
    {
        return input;
    }
    public String straction()
    {
        switch (super.getNumber())
        {
            case 1 : output = BlackFunctions.Function1(getinput());
                break;
            case 2 : output = BlackFunctions.Function2(getinput());
                break;
            case 3 : output = BlackFunctions.Function3(getinput());
                break;
            case 4 : output = BlackFunctions.Function4(getinput());
                break;
            case 5 : output = BlackFunctions.Function5(getinput());
                break;
        }
        return output;
    }
}
