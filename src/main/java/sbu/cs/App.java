package sbu.cs;

public class App {

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        Square[][] squares = new Square[n][n];
        setcolor(n , arr , squares);
        ((GreenSquare) squares[0][0]).setinput(input);
        firstrowaction(n , squares);
        betweenrowaction(n , squares);
        lastrowaction(n , squares);
        String ans = ((PinkSquare) squares[n-1][n-1]).straction();
        return ans;
    }
    public static void firstrowaction(int n , Square[][] squares )
    {
        String output = ((GreenSquare) squares[0][0]).straction();
        ((GreenSquare) squares[0][1]).setinput(output);
        ((GreenSquare) squares[1][0]).setinput(output);
        for(int i=1 ; i<n-2 ; i++)
        {
            String outPut = ((GreenSquare) squares[0][i]).straction();
            ((GreenSquare) squares[0][i+1]).setinput(outPut);
            ((BlueSquare) squares[1][i]).setupinput(outPut);
        }
        String OutPut = ((GreenSquare) squares[0][n-2]).straction();
        ((YellowSquare) squares[0][n-1]).setinput(OutPut);
        ((BlueSquare) squares[1][n-2]).setupinput(OutPut);
        String outPut = ((YellowSquare) squares[0][n-1]).straction();
        ((PinkSquare) squares[1][n-1]).setupinput(outPut);
    }
    public static void betweenrowaction(int n , Square[][] squares )
    {
        for(int i=1 ; i<n-1 ; i++)
        {
            for(int j=0 ; j<=n-1 ; j++)
            {
                if(j==0 && i<n-2)
                {
                    String output = ((GreenSquare) squares[i][j]).straction();
                    ((BlueSquare) squares[i][j+1]).setleftinput(output);
                    ((GreenSquare) squares[i+1][j]).setinput(output);
                }
                if(j>0 && i<n-2 && j<n-2)
                {
                    String output = ((BlueSquare) squares[i][j]).stractionleft();
                    String output2 = ((BlueSquare) squares[i][j]).stractionup();
                    ((BlueSquare) squares[i][j+1]).setleftinput(output);
                    ((BlueSquare) squares[i+1][j]).setupinput(output2);
                }
                if(i==n-2 && j<n-2 && j!=0)
                {
                    String output = ((BlueSquare) squares[i][j]).stractionleft();
                    String output2 = ((BlueSquare) squares[i][j]).stractionup();
                    ((BlueSquare) squares[i][j+1]).setleftinput(output);
                    ((PinkSquare) squares[i+1][j]).setupinput(output2);
                }
                if(j==n-2 && i<n-2)
                {
                    String output = ((BlueSquare) squares[i][j]).stractionleft();
                    String output2 = ((BlueSquare) squares[i][j]).stractionup();
                    ((BlueSquare) squares[i+1][j]).setupinput(output2);
                    ((PinkSquare) squares[i][j+1]).setleftinput(output);
                }
                if(i==n-2 && j==n-2)
                {
                    String output = ((BlueSquare) squares[i][j]).stractionleft();
                    String output2 = ((BlueSquare) squares[i][j]).stractionup();
                    ((PinkSquare) squares[i][j+1]).setleftinput(output);
                    ((PinkSquare) squares[i+1][j]).setupinput(output2);
                }
                if(j==n-1 && i<n-1)
                {
                    String output = ((PinkSquare) squares[i][j]).straction();
                    ((PinkSquare) squares[i+1][j]).setupinput(output);
                }
                if(i==n-2 && j==0)
                {
                    String output = ((GreenSquare) squares[i][j]).straction();
                    ((BlueSquare) squares[i][j+1]).setleftinput(output);
                    ((YellowSquare) squares[i+1][j]).setinput(output);
                }
            }
        }
    }
    public static void lastrowaction(int n , Square[][] squares )
    {
        String output = ((YellowSquare) squares[n-1][0]).straction();
        ((PinkSquare) squares[n-1][1]).setleftinput(output);
        for(int i=1 ; i<n-1 ; i++) {
            String outPut = ((PinkSquare) squares[n - 1][i]).straction();
            ((PinkSquare) squares[n - 1][i + 1]).setleftinput(outPut);
        }
    }
    public static void setcolor(int n, int[][] arr,Square[][] squares)
    {
        for(int i=0 ; i<n ; i++)
        {
            for(int j=0 ; j<n ; j++)
            {
                if(i==0 && j<n-1)
                {
                    GreenSquare square = new GreenSquare(arr[i][j]);
                    squares[i][j]=square;
                }
                else if(j==0 && i<n-1)
                {
                    GreenSquare square = new GreenSquare(arr[i][j]);
                    squares[i][j]=square;
                }
                else if(i==n-1 && j>0)
                {
                    PinkSquare square = new PinkSquare(arr[i][j]);
                    squares[i][j]=square;
                }
                else if(j==n-1 && i>0)
                {
                    PinkSquare square = new PinkSquare(arr[i][j]);
                    squares[i][j]=square;
                }
                else if((i==0 && j==n-1) || (j==0 && i==n-1))
                {
                    YellowSquare square = new YellowSquare(arr[i][j]);
                    squares[i][j]=square;
                }
                else
                {
                    BlueSquare square = new BlueSquare(arr[i][j]);
                    squares[i][j]=square;
                }
            }
        }
    }
}
