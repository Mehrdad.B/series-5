package sbu.cs;

public class PinkSquare extends Square{
    public PinkSquare(int functionNumber) {
        super(functionNumber);
    }
    private String uptinput;
    private String leftinput;
    private String output;
    public void setupinput(String input)
    {
        uptinput=input;
    }
    public void setleftinput(String input)
    {
        leftinput=input;
    }
    public String getUptinput()
    {
        return uptinput;
    }
    public String getLefttinput()
    {
        return leftinput;
    }
    public String straction()
    {
        switch (super.getNumber())
        {
            case 1 : output = WhiteFunctions.Function1(getLefttinput() , getUptinput());
                break;
            case 2 : output = WhiteFunctions.Function2(getLefttinput() , getUptinput());
                break;
            case 3 : output = WhiteFunctions.Function3(getLefttinput() , getUptinput());
                break;
            case 4 : output = WhiteFunctions.Function4(getLefttinput() , getUptinput());
                break;
            case 5 : output = WhiteFunctions.Function5(getLefttinput() , getUptinput());
                break;
        }
        return output;
    }
}
