package sbu.cs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BlackFunctionsTest {

    @Test
    void function1() {
        String tmp = BlackFunctions.Function1("kjhdkskfskfshfs");
        assertEquals(tmp , "sfhsfksfkskdhjk");
    }

    @Test
    void function2() {
        String tmp = BlackFunctions.Function2("alireza");
        assertEquals(tmp , "aalliirreezzaa");
    }

    @Test
    void function3() {
        String tmp = BlackFunctions.Function3("mohammadhossein");
        assertEquals(tmp , "mohammadhosseinmohammadhossein");
    }

    @Test
    void function4() {
        String tmp = BlackFunctions.Function4("katayoun");
        assertEquals(tmp , "nkatayou");
    }

    @Test
    void function5() {
        String tmp = BlackFunctions.Function5("mojtaba");
        assertEquals(tmp , "nlqgzyz");
    }
}