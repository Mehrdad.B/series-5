package sbu.cs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WhiteFunctionsTest {

    @Test
    void function1() {
        String tmp = WhiteFunctions.Function1("mehrdad" , "hasan");
        assertEquals(tmp , "mheahsradnad");
    }

    @Test
    void function2() {
        String tmp = WhiteFunctions.Function2("reyhaneh" , "peyman");
        assertEquals(tmp , "reyhanehnamyep");
    }

    @Test
    void function3() {
        String tmp = WhiteFunctions.Function3("kimiya" , "behzad");
        assertEquals(tmp , "kdiamzihyeab");
    }

    @Test
    void function4() {
        String tmp = WhiteFunctions.Function4("morteza" , "mahyar");
        assertEquals(tmp , "mahyar");
    }

    @Test
    void function5() {
        String tmp = WhiteFunctions.Function5("ali" , "mehrab");
        assertEquals(tmp , "mpprab");
    }
}